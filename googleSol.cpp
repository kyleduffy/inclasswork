
// Kyle Duffy - kduffy5

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>

std::unordered_map<char,int> getWordHash(std::string word) {
    std::unordered_map<char,int> hashedWord;
    for (char letter : word) {
        if (hashedWord.count(letter))
            hashedWord[letter]++;
        else
            hashedWord[letter] = 1;
    }
    return hashedWord;
}

std::string parsePlate(std::string plateContents) {
    std::string lettersInPlate = "";
    for (char character : plateContents){
        if (character >= 65 && character <= 90) {
            lettersInPlate += character;
        }
    }
    return lettersInPlate;
}

bool checkForMatch(std::unordered_map<char,int> plateMap, std::string word) {
    std::unordered_map<char,int> wordMap = getWordHash(word);
    for (auto letterCountPair : plateMap) {
        if (plateMap.count(letterCountPair.second))
            return false;
        else {
            if (wordMap[letterCountPair.first] < letterCountPair.second)
                return false;
        }
    }
    return true;
}

int main() {

    std::string fileName = "words.txt";
    // std::string fileName;
    bool CONFIGURABLE = false;
    if (CONFIGURABLE) {
        std::cout << "Dictionary: ";
        std::cin >> fileName;
    }
    std::ifstream dictionaryFileStream(fileName);

    std::string plate;
    std::cout << "License Plate: ";
    std::cin >> plate;
    std::unordered_map<char,int> plateMap = getWordHash(parsePlate(plate));

    std::string word;
    std::string currentBestMatch = "";
    while (dictionaryFileStream >> word) {
        if (!currentBestMatch.empty() && word.length() >= currentBestMatch.length())
            continue;
        else {
            if (checkForMatch(plateMap, word))
                currentBestMatch = word;
        }
    }

    if (currentBestMatch.empty())
        std::cout << "No match found." << std::endl;
    else
        std::cout << "Shortest word: " << currentBestMatch << std::endl;

	return 0;
}
