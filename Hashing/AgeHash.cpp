<<<<<<< HEAD
/**********************************************
* File: AgeHash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows the comparison of the input or ordered and
* unordered sets 
**********************************************/
// Libraries Here
#include <map>
#include <unordered_map>
#include <iterator>
#include <iostream>
#include <string>

using namespace std;

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(int argc, char** argv){

    map<string, int> ageHashOrdered = { {"Matthew", 38}, {"Alfred", 72}, {"Roscoe", 36}, {"James", 35} };
    map<string, int>::iterator iterOr;
	
    cout << "The ordered hashed are: " << endl;

    for (iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++) {
        cout << iterOr->first << " " << iterOr->second << endl;
    }  

    unordered_map<string, int> ageHashUnordered = { {"Matthew", 38}, {"Alfred", 72}, {"Roscoe", 36}, {"James", 35} };
    unordered_map<string, int>::iterator iterUn;
    
    cout << "+-----+" << endl << "The unordered hashes are:" << endl;
    for (iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); iterUn++) {
        cout << iterUn->first << " " << iterUn->second << endl;
    }

    cout << "The ordered example: " << ageHashOrdered["Matthew"] << endl;
    cout << "The unordered example: " << ageHashUnordered["James"] << endl;

	return 0;
}
/*
=======
/**********************************************
* File: AgeHash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows the comparison of the input or ordered and
* unordered sets 
**********************************************
// Libraries Here

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution  
********************************************
int main(int argc, char** argv){

	
	return 0;
}
>>>>>>> dd246b4b3a1d6d7525ac56420b85e58718d89e26
*/
