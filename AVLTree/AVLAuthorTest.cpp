/**********************************************
* File: AVLAuthorTest.cpp
* Author: 
* Email: 
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

using namespace std;

// Struct goes here

struct Author {
    string first;
    string last;

    Author(string first, string last) : first(first), last(last) {}

    bool operator<(const Author& rhs) const {
        if (last < rhs.last) { return true; }
        else if (last == rhs.last) { return (first < rhs.first); }
        return false;
    }

    bool operator==(const Author& rhs) const {
        if (last != rhs.last) { return false; }
        else if (first != rhs.first) { return false; }
        return true;
    } // can be made one line with return (first == rhs.first && last == rhs.last);

    friend ostream& operator<<(ostream& os, const Author& printAuth);

};

ostream& operator<<(ostream& os, const Author& printAuth) {
    os << printAuth.last << ", " << printAuth.first;
    return os;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
    
    AVLTree<Author> authorsAVL;

    Author A("Anthony", "Aardvark");
    Author B("Gregory", "Aardvark");
    Author C("Michael", "Main");
    Author D("Gayle", "McDowell");
    Author BadPerson("BadMean", "Person");
    Author E("Walter", "Savitch");

    authorsAVL.insert(A);
    authorsAVL.insert(B);
    authorsAVL.insert(C);
    authorsAVL.insert(D);
    authorsAVL.insert(BadPerson);
    authorsAVL.insert(E);

    authorsAVL.printTree();
	
    cout << "Removing " << BadPerson << endl;
    authorsAVL.remove(BadPerson);
    authorsAVL.printTree();
    
    return 0;
}
