/**********************************************
* File: 5b06.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#include<iostream>
using namespace std;

struct node{
    int data;
    node *right, *left;
};

/********************************************
* Function Name  : createBst
* Pre-conditions : node* &root, int arr[], int start, int end
* Post-conditions: none
*  
********************************************/
void createBst(node* &root, int arr[], int start, int end){
    if(start>end)
        return;
    if(root==NULL){
        node *ptr = new node;
        int ind = start + (end-start)/2;
        ptr->data = arr[ind];
        ptr->left = NULL;
        ptr->right = NULL;
        root = ptr;
        createBst(root->left, arr, start, ind-1);
        createBst(root->right, arr, ind+1, end);
    }
}

/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
*  
********************************************/
int main(){
    int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    node* root;
    root = NULL;
    createBst(root, arr, 0, 8);
    cout<<root->left->data<<" "<<root->data<<" "<<root->right->data<<'\n';
}
