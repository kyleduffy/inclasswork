/*************************************
 * Filename: Section01_Rule3Y.cc 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 * 
 * This file contains an IntVec class
 * that meets the "Rule of 3"
 * **********************************/

#include<iostream>

class IntVec {
public:

    /***********************
     * Function Name: IntVec
     * Preconditions: int
     * Postconditions: none
     * This is the IntVec constructor
     * *********************/
   IntVec(int n): data(new int[n]), size(n) { }
   
   /***********************
     * Function Name: ~IntVec
     * Preconditions: none
     * Postconditions: none
     * This is the IntVec destructor
     * *********************/
   ~IntVec() { 
       delete[] data; 
    }
   
   /***********************
     * Function Name: operator[]
     * Preconditions: int
     * Postconditions: int &
     * Returns the address of the nth value
     * of the array
     * *********************/
   int& operator[](int n){ 
       return data[n]; 
   }
   
   /***********************
     * Function Name: operator []
     * Preconditions: int
     * Postconditions: const int&
     * 
     * Returns a constant address to the nth
     * element of the data array
     * *********************/
   const int& operator[](int n) const{ 
       return data[n]; 
   }

    /***********************
     * Function Name: IntVec
     * Preconditions: const IntVec& v
     * Postconditions: none
     * This is the IntVec copy constructor
     * *********************/
   IntVec(const IntVec& v):
      data(new int[v.size]),
      size(v.size) {
      std::copy(data, data + size, v.data);
   }
   
     /***********************
     * Function Name: operator=
     * Preconditions: const IntVec& v
     * Postconditions: IntVec&
     * This is the IntVec assignment operator
     * *********************/  
   IntVec& operator=(const IntVec& v) {
      int* newdata = new int[v.size];
      std::copy(v.data,v.data+v.size, newdata);
      delete[] data;
      data = newdata;
      size = v.size;
      return *this;
   }

private:
   int* data;
   int size;
};

/****************
 * Function Name: main
 * Preconditions: int, char**
 * Postconditions: int
 * This is the main driver Function
 * ***************/
int main(int argc, char **argv)
{
   IntVec *x = new IntVec(100);
   IntVec *y = x;
   
   std::cout << "The " << 5 << "th value of IntVec x is " << (*x)[5] << std::endl;
   std::cout << "The " << 5 << "th value of IntVec y is " << (*y)[5] << std::endl;
   
   delete x;
   // delete y; Not necessary, since y is pointing to x
   return 0;
}